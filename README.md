Manuel d'utilisateur:

1. Appuyez "N" pour créer un nouveau message et fichier source.
2. Mettez le message sans espaces et le nom du fichier en forme (nom).txt
3. Appuyez "C" pour commencer l'encryptage.
4. Choisissez un perroquet.
5. Appuyez "D" pour commencer le décryptage.
6. Saisissez le perroquet correct.
7. Le résultat du décryptage est sauvegardé dans le fichier resultat.txt

Lien GitLab: https://gitlab.com/astrorges/cryptage/-/commits/master