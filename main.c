#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100

#include "fonctions.h"


int main()
{
    /* Déclaration des tableaux de caractères  */
    char message[MAX_SIZE];
    char fichierSource[MAX_SIZE];
    char original[MAX_SIZE];
    char motCrypte[MAX_SIZE];
    char motDecrypte[MAX_SIZE];
    char luCrypte[MAX_SIZE];
    char perroquet[MAX_SIZE];
    char choix;
    int lecture;
    int longeurMessage = 0;
    int longeurPerroquet = 0;

    /* Lancement de l'interface  */
    while (1) {
        fflush(stdin);
        /* Choisir l'action */
        printf("\nChoisissez votre action:\n \"N\" pour creer un nouveau message \n \"C\" pour le cryptage \n \"D\" pour le decryptage \n \"E\" pour sortir du programme \n");
        printf("\nVotre choix: ");
        scanf("%c", &choix);

        /* Création du message et du fichier source  */
        if(choix == 'N') {


            printf("\nInserez un message a crypter (sans espaces): ");
            scanf("%s", &message);

            longeurMessage = strlen(message);

            printf("\nNommez le fichier qui contiendra votre message (<nom>.txt): ");
            scanf("%s", &fichierSource);

            ecrireFichier(fichierSource, message, longeurMessage);

            printf("\nUn nouveau fichier %s contenant votre message a ete cree!\n", fichierSource);

            continue;
        }

         /* Cryptage */
         else if(choix == 'C') {

            /* Lecture du fichier source et récuperation de contenu */
            lecture = lireFichier(fichierSource, original, longeurMessage);
            if (lecture == 0) {
                printf("\nMessage original: %s\n", original);

                /* Creation du perroquet  */
                saisirPerroquet(perroquet, &longeurPerroquet);

                /* Cryptage du contenu de fichier source  */
                cryptage(original, motCrypte, perroquet, longeurPerroquet);
                printf("\nMessage crypte: %s\n", motCrypte);

                /* Création du fichier contenant le message crypté  */
                ecrireFichier("dest.crt", motCrypte, longeurMessage);

                printf("\nLe message a ete crypte!\n");

                /* Supprimer le fichier source  */

                supprimerFichier(fichierSource);

                continue;
            }
            else {
                printf("\nVeuillez creer un nouveau message\n");
                continue;
            }
         }

        /* Decryptage */
        else if(choix =='D') {

            /* Lecture du message crypté et récuperation de contenu */
            lecture = lireFichier("dest.crt", luCrypte, longeurMessage);
            if (lecture == 0) {

                 /* Décryptage du message crypté */
                decryptage(luCrypte, motDecrypte, longeurPerroquet);
                printf("\nMessage decrypte: %s\n", motDecrypte);

                printf("\nLe message a ete decrypte!\n");

                continue;
            }

             else {
                    printf("\nVeuillez crypter un nouveau message\n");
                    continue;
            }

        }

        else if(choix =='E') {
            /*Sortir de la boucle */
            printf("\nFermeture du programme\n");
            break;
        }

        else {
            printf("\nSaisie incorrect, choisissez N (nouveau message), C (cryptage), D (decryptage) ou E (sortie)\n");
            continue;
        }

    }
    return 0;
}
