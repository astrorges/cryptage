#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

/* Lecture du fichier */
int lireFichier(char* nomFichier, char *mot, int longeurMessage) {
    FILE *source;
    source = fopen(nomFichier, "r");
    char line[MAX_SIZE];

    if (source == NULL)
    {
        printf("\nLe fichier est introuvable\n");
        return 1;
    }

    for (int i = 0; i < longeurMessage; i++) {
        char c = fgetc(source);
        mot[i] = (char)c;
    }
    fclose(source);
    return 0;
}

/* Ecriture des fichiers */
void ecrireFichier(char *nomFichier, char *motCrypte, int longeurMessage) {
    FILE *destination;
    destination = fopen(nomFichier, "w+t");
    for (int i = 0; i < longeurMessage; i++) {
        fputc(motCrypte[i], destination);
    }
    fclose(destination);
}

/* Suppression du fichier */
void supprimerFichier(char* nomFichier) {
    if(remove(nomFichier) == 0) {
        printf("\nLe fichier %s a ete supprime\n", nomFichier);
        } else {
        printf("\nLe fichier n'as pas ete supprime\n");
    }
}

/* Creation du perroquet */
char saisirPerroquet(char *perroquet, int *longeurPerroquet) {
    printf("\nSaisir le perroquet: ");
    scanf("%s", perroquet);
    *longeurPerroquet = strlen(perroquet);
    return perroquet;
}

/* Cryptage et creation du fichier peroq.def */
char cryptage(char *original, char *motCrypte, char *perroquet, int longeurPerroquet) {

    ecrireFichier("peroq.def", perroquet, longeurPerroquet);
    int nOriginal = strlen(original);
    int i = 0;
    int j = 0;
    while (i < nOriginal) {
        if (j > longeurPerroquet) {
            j = 0;
        }
        int diff = (int)original[i] - (int)perroquet[j];
        motCrypte[i] = (char)diff;
        i++;
        j++;
    }
    return motCrypte;
}

/* Decryptage et creation du fichier resultat.txt */
char decryptage(char* motCrypte, char* motDecrypte, int longeurPerroquet) {
     char perroquet[MAX_SIZE];
     char perroquetOriginal[MAX_SIZE];
     int tempLongPerroquet;

     lireFichier("peroq.def", perroquetOriginal, longeurPerroquet);

     while (1) {
         saisirPerroquet(perroquet, &tempLongPerroquet);
         if (strcmp(perroquet,perroquetOriginal) == 0){
            printf("\nLe perroquet est correct!\n");
            break;
         }
         else {
            printf("\nLe perroquet n'est pas correct. Veuillez saisir le perroquet de nouveau\n");
         }
     }

     int nCrypte = strlen(motCrypte);
     int i = 0;
     int j = 0;
     while (i < nCrypte) {
        if (j > longeurPerroquet) {
            j = 0;
        }
        int decryptDiff = (int)motCrypte[i] + (int)perroquet[j];
        motDecrypte[i] = (char)decryptDiff;
        i++;
        j++;
     }
     return motDecrypte;
}


#endif // FONCTIONS_H_INCLUDED
